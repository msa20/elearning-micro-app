import React, { Component } from 'react';
import { Provider } from 'mobx-react';
import { AppProps } from 'next/app';
import { store } from '@nextree-ms/quiz';
import { Tab, Tabs } from '@material-ui/core';


class ChannelApp extends Component<AppProps> {
  //
  render() {
    //
    const { Component, pageProps } = this.props;

    return (
      <Provider
        {...store}
      >
        <Tabs
          // value={value}
          indicatorColor="primary"
          textColor="primary"
          // onClick={(event) => this.onChangePage(event)}
          // onChange={onChange()}
          centered
        >
          <Tab label="출제" onClick={() => this.props.router.push('/instructor')} />
          <Tab label="응시" onClick={() => this.props.router.push('/student')} />
        </Tabs>
        <Component
          {...pageProps}
        />
      </Provider>
    );
  }
}

export default ChannelApp;
