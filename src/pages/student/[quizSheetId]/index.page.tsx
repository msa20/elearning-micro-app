import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import {
  SolvingQuizzesForm, QuizAnswerSheet,
} from '@nextree-ms/quiz';
import { observer } from 'mobx-react';
import { autobind, IdName, nWithStyles, NWithStyles, PageLayout, ReactComponent } from '~/comp/shared';


interface Props extends NWithStyles{
  router: NextRouter;
}

interface State {
}

@observer
@autobind
class TakeQuizPage extends ReactComponent<Props, State> {
  //
  onClickTakeQuiz(event: React.MouseEvent, quizSheetId: string) {
    //
    this.props.router.push(`/student/${quizSheetId}`);
  }

  getQuizSheetId() {
    //
    const { router } = this.props;
    const { quizSheetId } = router.query;

    return quizSheetId as string;
  }

  onSuccessSubmit(quizAnswerSheet: QuizAnswerSheet) {
    this.props.router.push('/student');
  }

  handleUnQualification() {
    alert('응시 할 수 없는 시험입니다.');
    this.props.router.push('/student');
  }

  render() {
    //
    const groupId = 'testExamineeGroup-1';
    const examinee = new IdName('testExaminee-1', '홍길동');

    return (
      <PageLayout>
        <PageLayout.Body>
          <PageLayout.Content>
            <SolvingQuizzesForm
              groupId={groupId}
              writer={examinee}
              quizSheetId={this.getQuizSheetId()}
              onSuccess={this.onSuccessSubmit}
              handleUnQualification={this.handleUnQualification}
            />
          </PageLayout.Content>
        </PageLayout.Body>
      </PageLayout>
    );
  }
}

export default withRouter(nWithStyles(TakeQuizPage));
