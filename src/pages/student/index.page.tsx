import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { Typography } from '@material-ui/core';
import { QuizSheetPublishedList } from '@nextree-ms/quiz';
import { observer } from 'mobx-react';
import { autobind, nWithStyles, NWithStyles, PageLayout, ReactComponent } from '~/comp/shared';


interface Props extends NWithStyles{
  router: NextRouter;
}

interface State {
}

@observer
@autobind
class StudentMainPage extends ReactComponent<Props, State> {
  //
  onClickTakeQuiz(event: React.MouseEvent, quizSheetId: string) {
    //
    this.props.router.push(`/student/${quizSheetId}`);
  }

  routeToQuizAnswerSheetList() {
    //
    this.props.router.push('/student/my-answer-sheet-list');
  }

  render() {
    //
    const { classes } = this.props;
    const groupId = 'testExamineeGroup-1';

    return (
      <PageLayout>
        <PageLayout.Header
          className={classes.ntr_simg07}
          title="시험 응시"
          description={'각 교육과정의 성취도를 확인 해보세요.'}
        />
        <PageLayout.Body>
          <PageLayout.Content>
            <QuizSheetPublishedList
              groupId={groupId}
              onClickButton={(event: React.MouseEvent, quizSheetId: string) => this.onClickTakeQuiz(event, quizSheetId)}
              // quizSheetId={this.quizSheetId}
              // writer={writer}
            />
            <Typography onClick={this.routeToQuizAnswerSheetList}>지난 퀴즈 보기</Typography>
          </PageLayout.Content>
        </PageLayout.Body>
      </PageLayout>
    );
  }
}

export default withRouter(nWithStyles(StudentMainPage));
