import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import {
  QuizAnswerSheet, QuizAnswerSheetDetail, QuizAnswerSheetList,
} from '@nextree-ms/quiz';
import { observer } from 'mobx-react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
} from '@material-ui/core';
import { autobind, IdName, nWithStyles, NWithStyles, PageLayout, ReactComponent } from '~/comp/shared';


interface Props extends NWithStyles{
  router: NextRouter;
}

interface State {
  open: boolean;
  selectedQuizAnswerSheetId: string;
}

@observer
@autobind
class MyAnswerSheetList extends ReactComponent<Props, State> {
  //
  state = { open: false, selectedQuizAnswerSheetId: '' };

  onClickTakeQuiz(event: React.MouseEvent, quizSheetId: string) {
    //
    this.props.router.push(`/student/${quizSheetId}`);
  }

  getQuizSheetId() {
    //
    const { router } = this.props;
    const { quizSheetId } = router.query;

    return quizSheetId as string;
  }

  onSuccessSubmit(quizAnswerSheet: QuizAnswerSheet) {
    this.props.router.push('/student');
  }

  handleUnQualification() {
    alert('응시 할 수 없는 시험입니다.');
    this.props.router.push('/student');
  }

  onClickAnswerSheet(event: React.MouseEvent, quizAnswerSheet: QuizAnswerSheet) {
    //
    this.setState({ open: true, selectedQuizAnswerSheetId: quizAnswerSheet.id });
  }

  handleClose() {
    this.setState({ open: false });
  }

  onClickBack() {
    //
    this.props.router.back();
  }

  render() {
    //
    const { open, selectedQuizAnswerSheetId } = this.state;
    const groupId = 'testExamineeGroup-1';
    const examinee = new IdName('testExaminee-1', '홍길동');

    return (
      <PageLayout>
        <PageLayout.Body>
          <PageLayout.Content>
            <QuizAnswerSheetList
              groupId={groupId}
              writer={examinee}
              onClick={this.onClickAnswerSheet}
            />
          </PageLayout.Content>
          <Button onClick={this.onClickBack}>뒤로가기</Button>
          <Dialog
            fullWidth
            maxWidth={'xl'}
            open={open}
            onClose={this.handleClose}
            aria-labelledby="max-width-dialog-title"
          >
            <DialogContent>
              <QuizAnswerSheetDetail
                groupId={groupId}
                quizAnswerSheetId={selectedQuizAnswerSheetId}
                writer={examinee}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Close
              </Button>
            </DialogActions>
          </Dialog>

        </PageLayout.Body>
      </PageLayout>
    );
  }
}

export default withRouter(nWithStyles(MyAnswerSheetList));
