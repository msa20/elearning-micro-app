import React from 'react';
import { ReactComponent } from '~/comp/shared';


class IndexPage extends ReactComponent {
  //
  render() {
    //
    return (
      <div>
        Hello, World!
      </div>
    );
  }
}

export default IndexPage;
