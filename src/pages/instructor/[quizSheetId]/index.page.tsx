import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { observer } from 'mobx-react';
import { Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import { autobind, IdName, nWithStyles, NWithStyles, PageLayout, ReactComponent } from '~/comp/shared';
import { QuizSheet, QuizSheetDetail } from '~/comp';



interface Props extends NWithStyles{
  router: NextRouter;
}

interface State {
  open: boolean;
}

@observer
@autobind
class QuizSheetDetailPage extends ReactComponent<Props, State> {
  //
  state = { open: false };

  onClickQuizSheet(event: React.MouseEvent, quizSheet: QuizSheet) {
    //
    this.props.router.push(`/instructor/${quizSheet.id}`);
  }

  getQuizSheetId() {
    //
    const { router } = this.props;
    const { quizSheetId } = router.query;

    return quizSheetId as string;
  }

  onSuccessFinish(quizSheet: QuizSheet) {
    //
    this.setState({ open: true });
  }

  hide() {
    //
    this.setState({ open: false });
  }

  render() {
    //
    const { classes } = this.props;
    const { open } = this.state;
    const writer = new IdName('testExaminer-1', '나무소리 코치');

    return (
      <PageLayout>
        <PageLayout.Header
          className={classes.ntr_simg07}
          title="시험지 출제"
          description={'각 과정의 성취도를 평가하기 위해 시험지를 만들어보세요.'}
        />
        <PageLayout.Body>
          <PageLayout.Content>
            <QuizSheetDetail
              writer={writer}
              quizSheetId={this.getQuizSheetId()}
              onSuccessFinish={this.onSuccessFinish}
            />
            <Dialog aria-labelledby="customized-dialog-title" open={open}>
              <DialogTitle id="customized-dialog-title">
                알림
              </DialogTitle>
              <DialogContent dividers>
                퀴즈가 출제되었습니다.
              </DialogContent>
            </Dialog>
          </PageLayout.Content>
        </PageLayout.Body>
      </PageLayout>
    );
  }
}

export default withRouter(nWithStyles(QuizSheetDetailPage));
