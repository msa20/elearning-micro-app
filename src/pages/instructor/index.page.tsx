import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { observer } from 'mobx-react';
import { Button, Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import { autobind, IdName, nWithStyles, NWithStyles, PageLayout, ReactComponent } from '~/comp/shared';
import { QuizSheet, QuizSheetForm, QuizSheetList } from '~/comp';



interface Props extends NWithStyles{
  router: NextRouter;
}

interface State {
  open: boolean;
  listInitTrigger: boolean;
}

@observer
@autobind
class InstructorMainPage extends ReactComponent<Props, State> {
  //
  state: State = {
    open: false,
    listInitTrigger: false,
  };

  handleClose() {
    this.hide();
  }

  onClick() {
    this.show();
  }

  show() {
    this.setState({ open: true });
  }

  hide() {
    this.setState({ open: false });
  }

  onSuccessRegister() {
    //
    this.setState({ listInitTrigger: !this.state.listInitTrigger });
    this.hide();
  }

  onFailRegister() {
    //
    alert('시험지 등록에 실패했습니다.');
    this.hide();
  }

  onChangePage(event: React.MouseEvent) {
    this.props.router.push('/student');
  }

  onClickQuizSheet(event: React.MouseEvent, quizSheet: QuizSheet) {
    //
    this.props.router.push(`/instructor/${quizSheet.id}`);
  }


  render() {
    //
    const { classes } = this.props;
    const { open, listInitTrigger } = this.state;

    const menuList = ['student', 'instructor'];

    const writer = new IdName('testWriter-1', '홍길동');
    const groupId = 'testExamineeGroup-1';

    return (
      <PageLayout>
        <PageLayout.Header
          className={classes.ntr_simg07}
          title="시험지 출제"
          description={'각 과정의 성취도를 평가하기 위해 시험지를 만들어보세요.'}
        />
        <PageLayout.Body>
          <PageLayout.Content>
            <QuizSheetList
              groupId={groupId}
              limit={10}
              initTrigger={listInitTrigger}
              additionalAction={
                <>
                  <Button variant="contained" onClick={this.onClick}>시험지 작성하기</Button>
                  <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={open}>
                    <DialogTitle id="customized-dialog-title">
                      시험지 작성
                    </DialogTitle>
                    <DialogContent dividers>
                      <QuizSheetForm
                        groupId={groupId}
                        writer={writer}
                        onSuccess={() => this.onSuccessRegister()}
                        onFail={() => this.onFailRegister()}
                      />
                    </DialogContent>
                  </Dialog>
                </>
              }
              onClick={this.onClickQuizSheet}
            />
          </PageLayout.Content>
        </PageLayout.Body>
      </PageLayout>
    );
  }
}

export default withRouter(nWithStyles(InstructorMainPage));
