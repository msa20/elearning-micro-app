import { makeAutoObservable, runInAction } from 'mobx';
import { QueryParam, Offset, Operator } from '~/comp/shared';

import { QuizSheet, QuizSheetQueryApiStub, QuizSheetsDynamicQuery } from '~/comp/api/quiz';


class QuizSheetsStateKeeper {
  static readonly instanceName: string = 'quizSheetsStateKeeper';
  static instance: QuizSheetsStateKeeper;

  private readonly quizSheetQueryApi: QuizSheetQueryApiStub;

  quizSheets: QuizSheet[] = [];

  totalCount: number = 0;

  constructor(quizSheetQueryApi: QuizSheetQueryApiStub = QuizSheetQueryApiStub.instance) {
    this.quizSheetQueryApi = quizSheetQueryApi;
    makeAutoObservable(this);
  }

  setQuizSheetProp(index: number, name: keyof QuizSheet, value: any) {
    if (!this.quizSheets || !this.quizSheets[index]) {
      throw new Error(`QuizSheetsStateKeeper.setQuizSheetProp -> quizSheets[${index}] is null`);
    }
    (this.quizSheets[index] as any)[name] = value;
  }

  clear() {
    this.quizSheets = [];
  }

  async findQuizSheets(groupId: string, offset: Offset): Promise<QuizSheet[]> {
    const quizSheetsDynamicQuery = QuizSheetsDynamicQuery.oneParam<QuizSheet[]>(
      QueryParam.endParam('groupId', Operator.Equal, groupId)
    );

    quizSheetsDynamicQuery.offset = offset;

    const quizSheets = await this.quizSheetQueryApi.executeQuizSheetsDynamicQuery(quizSheetsDynamicQuery);

    runInAction(() => {
      this.quizSheets = quizSheets;
    });

    return quizSheets;
  }

}

QuizSheetsStateKeeper.instance = new QuizSheetsStateKeeper();

export default QuizSheetsStateKeeper;

