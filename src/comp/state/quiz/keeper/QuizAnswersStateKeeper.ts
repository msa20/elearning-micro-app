import { makeAutoObservable, runInAction } from 'mobx';

import {
  QuizAnswer,
  QuizAnswerCdo,
  QuizAnswerQueryApiStub,
  QuizAnswersDynamicQuery,
  QuizSheetApiStub,
} from '~/comp/api/quiz';
import { CommandResponse, Offset, Operator, QueryParam } from '~/comp/shared';
import { QuizSheetFlowApiStub } from '~/comp/api/quiz-flow';


class QuizAnswersStateKeeper {
  static readonly instanceName: string = 'quizAnswersStateKeeper';
  static instance: QuizAnswersStateKeeper;

  private readonly quizAnswerApi: QuizSheetApiStub;
  private readonly quizAnswerQueryApi: QuizAnswerQueryApiStub;
  private readonly quizSheetFlowApi: QuizSheetFlowApiStub;

  quizAnswers: QuizAnswer[] = [];

  constructor(
    quizAnswerApi: QuizSheetApiStub = QuizSheetApiStub.instance,
    quizAnswerQueryApi: QuizAnswerQueryApiStub = QuizAnswerQueryApiStub.instance,
    quizSheetFlowApi: QuizSheetFlowApiStub = QuizSheetFlowApiStub.instance,
  ) {
    this.quizAnswerApi = quizAnswerApi;
    this.quizAnswerQueryApi = quizAnswerQueryApi;
    this.quizSheetFlowApi = quizSheetFlowApi;
    makeAutoObservable(this);
  }

  setQuizAnswerProp(index: number, name: keyof QuizAnswer, value: any) {
    if (!this.quizAnswers || !this.quizAnswers[index]) {
      throw new Error(`QuizAnswersStateKeeper.setQuizAnswerProp -> quizAnswers[${index}] is null`);
    }
    (this.quizAnswers[index] as any)[name] = value;
  }

  clear() {
    this.quizAnswers = [];
  }

  async save(quizAnswers: QuizAnswer[]): Promise<CommandResponse> {
    //
    const response = await this.register(QuizAnswerCdo.fromModels(quizAnswers));

    return response;
  }

  async register(quizAnswerCdos: QuizAnswerCdo[]): Promise<CommandResponse> {
    return this.quizAnswerApi.registerQuizAnswers(quizAnswerCdos);
  }

  async findQuizAnswersByQuizAnswerSheetId(quizAnswerSheetId: string, offset: Offset): Promise<QuizAnswer[]> {
    //
    const query = QuizAnswersDynamicQuery.oneParam<QuizAnswer[]>(
      QueryParam.endParam('quizAnswerSheetId', Operator.Equal, quizAnswerSheetId),
    );

    query.offset = offset;

    const quizAnswers = await this.quizAnswerQueryApi.executeQuizAnswersDynamicQuery(query);

    runInAction(() => {
      this.quizAnswers = quizAnswers;
    });

    return quizAnswers;
  }

  async submitQuiz(quizAnswerSheetId: string): Promise<CommandResponse> {
    return this.quizSheetFlowApi.submitQuiz(quizAnswerSheetId, this.quizAnswers);
  }

}

QuizAnswersStateKeeper.instance = new QuizAnswersStateKeeper();

export default QuizAnswersStateKeeper;
