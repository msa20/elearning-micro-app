import { CqrsDynamicQuery } from '~/comp/shared';
import { QuizAnswerSheet } from '../../api-model';


class QuizAnswerSheetDynamicQuery extends CqrsDynamicQuery<QuizAnswerSheet> {
}

export default QuizAnswerSheetDynamicQuery;
