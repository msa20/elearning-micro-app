import { CqrsDynamicQuery } from '~/comp/shared';
import { QuizSheet } from '../../api-model';


class QuizSheetDynamicQuery extends CqrsDynamicQuery<QuizSheet> {
}

export default QuizSheetDynamicQuery;
