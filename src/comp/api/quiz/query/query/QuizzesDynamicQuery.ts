import { CqrsDynamicQuery } from '~/comp/shared';
import { Quiz } from '../../api-model';


class QuizzesDynamicQuery extends CqrsDynamicQuery<Quiz[]> {
}

export default QuizzesDynamicQuery;
