import { CqrsDynamicQuery } from '~/comp/shared';
import { Quiz } from '../../api-model';


class QuizDynamicQuery extends CqrsDynamicQuery<Quiz> {
}

export default QuizDynamicQuery;
