import { CqrsDynamicQuery } from '~/comp/shared';
import { QuizSheet } from '../../api-model';


class QuizSheetsDynamicQuery extends CqrsDynamicQuery<QuizSheet[]> {
}

export default QuizSheetsDynamicQuery;
