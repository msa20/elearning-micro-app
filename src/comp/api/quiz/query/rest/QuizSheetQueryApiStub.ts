import axios from 'axios';
import {
  QuizSheetDynamicQuery,
  QuizSheetQuery,
  QuizSheetsDynamicQuery,
} from '../query';
import { QuizSheet } from '../../api-model';


class QuizSheetQueryApiStub {
  static instance: QuizSheetQueryApiStub;

  private readonly quizSheetQueryApiUri: string = '/api/quiz/secure/quiz-sheet/query';

  async executeQuizSheetQuery(query: QuizSheetQuery): Promise<QuizSheet> {
    return axios.post(`${this.quizSheetQueryApiUri}/`, query)
      .then((response) => response && response.data && response.data.queryResult && QuizSheet.fromDomain(response.data.queryResult));
  }

  async executeQuizSheetDynamicQuery(query: QuizSheetDynamicQuery): Promise<QuizSheet | null> {
    return axios.post(`${this.quizSheetQueryApiUri}/dynamic-single`, query)
      .then((response) => response && response.data && response.data.queryResult && QuizSheet.fromDomain(response.data.queryResult));
  }

  async executeQuizSheetsDynamicQuery(query: QuizSheetsDynamicQuery): Promise<QuizSheet[]> {
    return axios.post(`${this.quizSheetQueryApiUri}/dynamic-multi`, query)
      .then((response) => response && response.data && QuizSheet.fromDomains(response.data.queryResult));
  }
}

QuizSheetQueryApiStub.instance = new QuizSheetQueryApiStub();

export default QuizSheetQueryApiStub;
