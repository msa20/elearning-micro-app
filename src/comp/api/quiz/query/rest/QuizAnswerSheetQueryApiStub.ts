import axios from 'axios';
import {
  QuizAnswerSheetDynamicQuery,
  QuizAnswerSheetQuery,
  QuizAnswerSheetsDynamicQuery,
} from '../query';
import { QuizAnswerSheet } from '../../api-model';


class QuizAnswerSheetQueryApiStub {
  static instance: QuizAnswerSheetQueryApiStub;

  private readonly quizAnswerSheetQueryApiUri: string = '/api/quiz/secure/quiz-answer-sheet/query';

  async executeQuizAnswerSheetQuery(query: QuizAnswerSheetQuery): Promise<QuizAnswerSheet> {
    return axios.post(`${this.quizAnswerSheetQueryApiUri}/`, query)
      .then((response) => response && response.data && response.data.queryResult && QuizAnswerSheet.fromDomain(response.data.queryResult));
  }

  async executeQuizAnswerSheetDynamicQuery(query: QuizAnswerSheetDynamicQuery): Promise<QuizAnswerSheet | null> {
    return axios.post(`${this.quizAnswerSheetQueryApiUri}/dynamic-single`, query)
      .then((response) => response && response.data && response.data.queryResult && QuizAnswerSheet.fromDomain(response.data.queryResult));
  }

  async executeQuizAnswerSheetsDynamicQuery(query: QuizAnswerSheetsDynamicQuery): Promise<QuizAnswerSheet[]> {
    return axios.post(`${this.quizAnswerSheetQueryApiUri}/dynamic-multi`, query)
      .then((response) => response && response.data && QuizAnswerSheet.fromDomains(response.data.queryResult));
  }
}

QuizAnswerSheetQueryApiStub.instance = new QuizAnswerSheetQueryApiStub();

export default QuizAnswerSheetQueryApiStub;
