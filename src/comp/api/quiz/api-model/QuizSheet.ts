import { computed, makeObservable, observable } from 'mobx';
import { DomainEntity, IdName, NameValueList } from '~/comp/shared';
import { QuizSheetCategory, QuizSheetState } from './vo';


class QuizSheet extends DomainEntity {
  groupId: string;
  title: string;
  subject: string;
  writer: IdName | null = null;
  quizSheetCategory: QuizSheetCategory | null;
  quizSheetState: QuizSheetState;

  // only ui
  editing?: boolean;

  constructor(groupId: string, title: string, subject: string, quizSheetCategory: QuizSheetCategory | null, quizSheetState: QuizSheetState, editing?: boolean) {
    super();
    this.groupId = groupId;
    this.title = title;
    this.subject = subject;
    this.quizSheetCategory = quizSheetCategory;
    this.quizSheetState = quizSheetState;

    this.editing = editing;

    makeObservable(this, {
      groupId: observable,
      title: observable,
      subject: observable,
      writer: observable,
      quizSheetCategory: observable,
      quizSheetState: observable,
      editing: observable,
      writerName: computed,

    });
  }

  static fromDomain(domain: QuizSheet): QuizSheet {
    const quizSheet = new QuizSheet(
      domain.groupId,
      domain.title,
      domain.subject,
      domain.quizSheetCategory,
      domain.quizSheetState,
    );

    quizSheet.setDomainEntity(domain);
    quizSheet.writer = domain.writer;
    return quizSheet;
  }

  static fromDomains(domains: QuizSheet[]): QuizSheet[] {
    return domains.map(domain => this.fromDomain(domain));
  }

  static asNameValues(model: QuizSheet): NameValueList {
    return {
      nameValues: [
        {
          name: 'title',
          value: String(model.title),
        },
        {
          name: 'subject',
          value: String(model.subject),
        },
        {
          name: 'quizSheetCategory',
          value: String(model.quizSheetCategory),
        },
        {
          name: 'quizSheetState',
          value: String(model.quizSheetState),
        },
      ],
    };
  }

  static new(groupId: string): QuizSheet {
    return new QuizSheet(groupId, '', '', null, QuizSheetState.Working);
  }


  static newDefault(groupId: string, writer: IdName): QuizSheet {
    //
    const quizSheet = new QuizSheet(groupId, '', '', null, QuizSheetState.Working);

    quizSheet.writer = writer;

    return quizSheet;
  }


  get writerId() {
    //
    return this.writer && this.writer.id || '';
  }

  get writerName() {
    //
    return this.writer && this.writer.name || '';
  }
}

export default QuizSheet;
