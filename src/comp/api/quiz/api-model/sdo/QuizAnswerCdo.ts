import { IdName, validationUtils } from '~/comp/shared';
import { QuizGradingResult } from '../vo';
import QuizAnswer from '../QuizAnswer';


class QuizAnswerCdo {
  quizId: string;
  quizAnswerSheetId: string;
  quizGradingResult: QuizGradingResult;
  writer: IdName;

  constructor(
    quizId: string,
    quizAnswerSheetId: string,
    quizGradingResult: QuizGradingResult,
    writer: IdName,
  ) {
    this.quizId = quizId;
    this.quizAnswerSheetId = quizAnswerSheetId;
    this.quizGradingResult = quizGradingResult;
    this.writer = writer;
  }

  static fromModel(domain: QuizAnswer) {
    const params = validationUtils.checkNullableParams<QuizAnswer, keyof QuizAnswer>(
      domain,
      [
        'writer',
      ]
    );

    return new QuizAnswerCdo(
      domain.quizId,
      domain.quizAnswerSheetId,
      domain.quizGradingResult,
      params.writer,
    );
  }

  static fromModels(domains: QuizAnswer[]) {
    return domains.map(domain => this.fromModel(domain));
  }
}

export default QuizAnswerCdo;
