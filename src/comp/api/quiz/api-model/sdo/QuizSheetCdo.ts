import { IdName, validationUtils } from '~/comp/shared';
import { QuizSheetCategory, QuizSheetState } from '../vo';
import QuizSheet from '../QuizSheet';


class QuizSheetCdo {
  groupId: string;
  title: string;
  subject: string;
  writer: IdName;
  quizSheetCategory: QuizSheetCategory;
  quizSheetState: QuizSheetState;

  constructor(
    groupId: string,
    title: string,
    subject: string,
    writer: IdName,
    quizSheetCategory: QuizSheetCategory,
    quizSheetState: QuizSheetState,
  ) {
    this.groupId = groupId;
    this.title = title;
    this.subject = subject;
    this.writer = writer;
    this.quizSheetCategory = quizSheetCategory;
    this.quizSheetState = quizSheetState;
  }

  static fromModel(domain: QuizSheet) {
    const params = validationUtils.checkNullableParams<QuizSheet, keyof QuizSheet>(
      domain,
      [
        'writer',
        'quizSheetCategory',
      ]
    );

    return new QuizSheetCdo(
      domain.groupId,
      domain.title,
      domain.subject,
      params.writer,
      params.quizSheetCategory,
      domain.quizSheetState,
    );
  }
}

export default QuizSheetCdo;
