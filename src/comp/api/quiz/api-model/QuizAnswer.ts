import { makeObservable, observable } from 'mobx';
import { DomainEntity, IdName, NameValueList } from '~/comp/shared';
import { QuizGradingResult } from './vo';


class QuizAnswer extends DomainEntity {
  quizId: string;
  quizAnswerSheetId: string;
  quizNo: string;
  quizGradingResult: QuizGradingResult;

  itemSeq: string;
  writer: IdName | null = null;

  constructor(quizId: string, quizAnswerSheetId: string, quizNo: string, quizGradingResult: QuizGradingResult, itemSeq: string) {
    super();
    this.quizId = quizId;
    this.quizAnswerSheetId = quizAnswerSheetId;
    this.quizNo = quizNo;
    this.quizGradingResult = quizGradingResult;
    this.itemSeq = itemSeq;

    makeObservable(this, {
      quizId: observable,
      quizAnswerSheetId: observable,
      quizNo: observable,
      quizGradingResult: observable,
      itemSeq: observable,
      writer: observable,
    });
  }

  static fromDomain(domain: QuizAnswer): QuizAnswer {
    const quizAnswer = new QuizAnswer(
      domain.quizId,
      domain.quizAnswerSheetId,
      domain.quizNo,
      domain.quizGradingResult,
      domain.itemSeq,
    );

    quizAnswer.setDomainEntity(domain);
    quizAnswer.writer = domain.writer;
    return quizAnswer;
  }

  static fromDomains(domains: QuizAnswer[]): QuizAnswer[] {
    return domains.map(domain => this.fromDomain(domain));
  }

  static new(quizId: string): QuizAnswer {
    return new QuizAnswer(quizId, '', '0', QuizGradingResult.InCorrect, '');
  }

  static asNameValues(model: QuizAnswer): NameValueList {
    return {
      nameValues: [
        {
          name: 'itemSeq',
          value: String(model.itemSeq),
        },
        {
          name: 'quizGradingResult',
          value: String(model.quizGradingResult),
        },
      ],
    };
  }
}

export default QuizAnswer;
