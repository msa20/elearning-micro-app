enum QuizSheetState {
  Working = '출제중',
  Published = '출제완료',
}

export default QuizSheetState;
