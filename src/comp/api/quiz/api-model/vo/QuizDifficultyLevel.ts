enum QuizDifficultyLevel {
  High = 'High',
  Average = 'Average',
  Low = 'Low',
}

export default QuizDifficultyLevel;
