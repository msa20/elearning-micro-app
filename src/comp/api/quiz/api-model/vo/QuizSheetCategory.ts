enum QuizSheetCategory {
  JavaScript = 'JavaScript',
  TypeScript = 'TypeScript',
  React = 'React',
  Mobx = 'Mobx',
  Java = 'Java',
  UML = 'UML',
  SpringBoot = 'SpringBoot',
  MicroService = 'MicroService',
  Etc = 'Etc',
}

export default QuizSheetCategory;
