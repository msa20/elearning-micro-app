import { makeObservable, observable } from 'mobx';


class QuizItem {
  sequence: number;
  itemText: string;

  constructor(sequence: number, itemText: string) {
    this.sequence = sequence;
    this.itemText = itemText;

    makeObservable(this, {
      sequence: observable,
      itemText: observable,
    });
  }

  static fromDomain(domain: QuizItem): QuizItem {
    return new QuizItem(
      domain.sequence,
      domain.itemText,
    );
  }

  static fromDomains(domains: QuizItem[]): QuizItem[] {
    return domains.map(domain => this.fromDomain(domain));
  }

  static new(): QuizItem {
    return new QuizItem(1, '');
  }

}

export default QuizItem;
