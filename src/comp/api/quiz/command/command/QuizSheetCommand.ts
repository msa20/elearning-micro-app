import { CqrsBaseCommand, CqrsBaseCommandType, NameValueList } from '~/comp/shared';
import { QuizSheetCdo } from '../../api-model';


class QuizSheetCommand extends CqrsBaseCommand {
  quizSheetCdo: QuizSheetCdo | null = null;
  quizSheetCdos: QuizSheetCdo[] = [];
  quizSheetId: string | null = null;
  nameValues: NameValueList | null = null;

  static newRegisterQuizSheetCommand(quizSheetCdo: QuizSheetCdo): QuizSheetCommand {
    const command = new QuizSheetCommand(CqrsBaseCommandType.Register);

    command.quizSheetCdo = quizSheetCdo;
    return command;
  }

  static newRegisterQuizSheetCommands(quizSheetCdos: QuizSheetCdo[]): QuizSheetCommand {
    const command = new QuizSheetCommand(CqrsBaseCommandType.Register);

    command.quizSheetCdos = quizSheetCdos;
    return command;
  }

  static newModifyQuizSheetCommand(quizSheetId: string, nameValues: NameValueList): QuizSheetCommand {
    const command = new QuizSheetCommand(CqrsBaseCommandType.Modify);

    command.quizSheetId = quizSheetId;
    command.nameValues = nameValues;
    return command;
  }

  static newRemoveQuizSheetCommand(quizSheetId: string): QuizSheetCommand {
    const command = new QuizSheetCommand(CqrsBaseCommandType.Remove);

    command.quizSheetId = quizSheetId;
    return command;
  }
}

export default QuizSheetCommand;
