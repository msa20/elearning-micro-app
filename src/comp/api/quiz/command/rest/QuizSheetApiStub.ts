import axios from 'axios';
import { CommandResponse, NameValueList } from '~/comp/shared';
import {
  QuizAnswerCommand,
  QuizAnswerSheetCommand,
  QuizCommand,
  QuizSheetCommand,
} from '../command';
import { QuizAnswerCdo, QuizAnswerSheetCdo, QuizCdo, QuizSheetCdo } from '../../api-model';


class QuizSheetApiStub {
  static instance: QuizSheetApiStub;

  private readonly quizSheetApiUri: string = '/api/quiz/secure/quiz-sheet';

  async registerQuiz(quizCdo: QuizCdo): Promise<CommandResponse> {
    const command = QuizCommand.newRegisterQuizCommand(quizCdo);
    return this.executeQuiz(command);
  }

  async registerQuizSheet(quizSheetCdo: QuizSheetCdo): Promise<CommandResponse> {
    const command = QuizSheetCommand.newRegisterQuizSheetCommand(quizSheetCdo);
    return this.executeQuizSheet(command);
  }

  async registerQuizAnswer(quizAnswerCdo: QuizAnswerCdo): Promise<CommandResponse> {
    const command = QuizAnswerCommand.newRegisterQuizAnswerCommand(quizAnswerCdo);
    return this.executeQuizAnswer(command);
  }

  async registerQuizAnswerSheet(quizAnswerSheetCdo: QuizAnswerSheetCdo): Promise<CommandResponse> {
    const command = QuizAnswerSheetCommand.newRegisterQuizAnswerSheetCommand(quizAnswerSheetCdo);
    return this.executeQuizAnswerSheet(command);
  }

  async registerQuizs(quizCdos: QuizCdo[]): Promise<CommandResponse> {
    const command = QuizCommand.newRegisterQuizCommands(quizCdos);
    return this.executeQuiz(command);
  }

  async registerQuizSheets(quizSheetCdos: QuizSheetCdo[]): Promise<CommandResponse> {
    const command = QuizSheetCommand.newRegisterQuizSheetCommands(quizSheetCdos);
    return this.executeQuizSheet(command);
  }

  async registerQuizAnswers(quizAnswerCdos: QuizAnswerCdo[]): Promise<CommandResponse> {
    const command = QuizAnswerCommand.newRegisterQuizAnswerCommands(quizAnswerCdos);
    return this.executeQuizAnswer(command);
  }

  async registerQuizAnswerSheets(quizAnswerSheetCdos: QuizAnswerSheetCdo[]): Promise<CommandResponse> {
    const command = QuizAnswerSheetCommand.newRegisterQuizAnswerSheetCommands(quizAnswerSheetCdos);
    return this.executeQuizAnswerSheet(command);
  }

  async modifyQuiz(quizId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = QuizCommand.newModifyQuizCommand(quizId, nameValues);
    return this.executeQuiz(command);
  }

  async modifyQuizSheet(quizSheetId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = QuizSheetCommand.newModifyQuizSheetCommand(quizSheetId, nameValues);
    return this.executeQuizSheet(command);
  }

  async modifyQuizAnswer(quizAnswerId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = QuizAnswerCommand.newModifyQuizAnswerCommand(quizAnswerId, nameValues);
    return this.executeQuizAnswer(command);
  }

  async modifyQuizAnswerSheet(quizAnswerSheetId: string, nameValues: NameValueList): Promise<CommandResponse> {
    const command = QuizAnswerSheetCommand.newModifyQuizAnswerSheetCommand(quizAnswerSheetId, nameValues);
    return this.executeQuizAnswerSheet(command);
  }

  async removeQuiz(quizId: string): Promise<CommandResponse> {
    const command = QuizCommand.newRemoveQuizCommand(quizId);
    return this.executeQuiz(command);
  }

  async removeQuizSheet(quizSheetId: string): Promise<CommandResponse> {
    const command = QuizSheetCommand.newRemoveQuizSheetCommand(quizSheetId);
    return this.executeQuizSheet(command);
  }

  async removeQuizAnswer(quizAnswerId: string): Promise<CommandResponse> {
    const command = QuizAnswerCommand.newRemoveQuizAnswerCommand(quizAnswerId);
    return this.executeQuizAnswer(command);
  }

  async removeQuizAnswerSheet(quizAnswerSheetId: string): Promise<CommandResponse> {
    const command = QuizAnswerSheetCommand.newRemoveQuizAnswerSheetCommand(quizAnswerSheetId);
    return this.executeQuizAnswerSheet(command);
  }

  async executeQuiz(quizCommand: QuizCommand): Promise<CommandResponse> {
    //
    return axios.post(`${this.quizSheetApiUri}/quiz/command`, quizCommand)
      .then((response) => response && response.data && response.data.commandResponse);
  }

  async executeQuizSheet(quizSheetCommand: QuizSheetCommand): Promise<CommandResponse> {
    //
    return axios.post(`${this.quizSheetApiUri}/quiz-sheet/command`, quizSheetCommand)
      .then((response) => response && response.data && response.data.commandResponse);
  }

  async executeQuizAnswer(quizAnswerCommand: QuizAnswerCommand): Promise<CommandResponse> {
    //
    return axios.post(`${this.quizSheetApiUri}/quiz-answer/command`, quizAnswerCommand)
      .then((response) => response && response.data && response.data.commandResponse);
  }

  async executeQuizAnswerSheet(quizAnswerSheetCommand: QuizAnswerSheetCommand): Promise<CommandResponse> {
    //
    return axios.post(`${this.quizSheetApiUri}/quiz-answer-sheet/command`, quizAnswerSheetCommand)
      .then((response) => response && response.data && response.data.commandResponse);
  }
}

QuizSheetApiStub.instance = new QuizSheetApiStub();

export default QuizSheetApiStub;
