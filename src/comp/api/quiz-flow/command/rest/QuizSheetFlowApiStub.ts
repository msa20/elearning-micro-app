import axios from 'axios';
import { CommandResponse, IdName } from '~/comp/shared';
import { QuizAnswer } from '~/comp/api';
import { TakeQuizCommand, SubmitQuizCommand } from '../command';


class QuizSheetFlowApiStub {
  static instance: QuizSheetFlowApiStub;

  private readonly quizSheetFlowApiUri: string = '/api/quiz/secure/question-flow';

  async takeQuiz(quizSheetId: string, writer: IdName): Promise<CommandResponse> {
    const command = TakeQuizCommand.new(
      quizSheetId,
      writer,
    );
    return axios.post(`${this.quizSheetFlowApiUri}/take-quiz`, command)
      .then((response) => response && response.data && response.data.commandResponse);
  }

  async submitQuiz(quizAnswerSheetId: string, quizAnswers: QuizAnswer[]): Promise<CommandResponse> {
    const command = SubmitQuizCommand.new(
      quizAnswerSheetId,
      quizAnswers,
    );
    return axios.post(`${this.quizSheetFlowApiUri}/submit-quiz`, command)
      .then((response) => response && response.data && response.data.commandResponse);
  }

}

QuizSheetFlowApiStub.instance = new QuizSheetFlowApiStub();

export default QuizSheetFlowApiStub;

