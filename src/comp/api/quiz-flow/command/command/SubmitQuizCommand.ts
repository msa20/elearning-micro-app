import { CqrsUserCommand } from '~/comp/shared';
import { QuizAnswer } from '~/comp/api';


class SubmitQuizCommand extends CqrsUserCommand {
  quizAnswerSheetId: string;
  quizAnswers: QuizAnswer[];

  constructor(quizAnswerSheetId: string, quizAnswers: QuizAnswer[]) {
    super();
    this.quizAnswerSheetId = quizAnswerSheetId;
    this.quizAnswers = quizAnswers;
  }

  static new(quizAnswerSheetId: string, quizAnswers: QuizAnswer[]) {
    return new SubmitQuizCommand(
      quizAnswerSheetId,
      quizAnswers
    );
  }

}

export default SubmitQuizCommand;
