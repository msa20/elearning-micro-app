import React from 'react';
import { observer } from 'mobx-react';
import { Box, Table, Typography, TableRow, TableCell, TableBody, TableHead, Grid } from '@material-ui/core';

import { QuizSheet } from '~/comp/api/quiz';
import { ReactComponent } from '~/comp/shared';


interface Props {
  quizSheets: QuizSheet[];
  onClick: (event: React.MouseEvent, answer: QuizSheet) => void;
  additionalAction: React.ReactNode;
}

@observer
class QuizSheetListView extends ReactComponent<Props> {
  //
  render() {
    const { quizSheets, onClick, additionalAction } = this.props;

    return (
      <>
        <Grid container>
          <Grid item xs={6}>
            <Typography color="textSecondary" gutterBottom>총 {quizSheets.length}개</Typography>
          </Grid>
          <Grid item xs={6}>
            <Box pb={1} textAlign="right">
              {additionalAction}
            </Box>
          </Grid>
        </Grid>
        <Box mb={2}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell variant="head" align="center">
                  <Typography>제목</Typography>
                </TableCell>
                <TableCell variant="head" align="center">
                  <Typography>주제</Typography>
                </TableCell>
                <TableCell variant="head" align="center">
                  <Typography>카테고리</Typography>
                </TableCell>
                <TableCell variant="head" align="center">
                  <Typography>상태</Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {quizSheets.length ? quizSheets.map((quizSheet) => (
                <TableRow key={quizSheet.id} hover onClick={(event: React.MouseEvent) => onClick(event, quizSheet)}>
                  <TableCell align="center">
                    <Typography>{quizSheet.title}</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography>{quizSheet.subject}</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography>{quizSheet.quizSheetCategory}</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography>{quizSheet.quizSheetState}</Typography>
                  </TableCell>
                </TableRow>
              )) : (
                <TableRow>
                  <TableCell colSpan={4} align="center">등록된 시험지가 없습니다.</TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Box>
      </>
    );
  }
}

export default QuizSheetListView;

