import React from 'react';
import { inject, observer } from 'mobx-react';
import { Box, Button, Typography } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { Quiz, QuizItem } from '~/comp/api';
import { QuizStateKeeper, QuizzesStateKeeper } from '~/comp/state';
import { IdName, ReactComponent, Offset, validationUtils, autobind } from '~/comp/shared';
import QuizKeyName from './model/QuizKeyName';
import QuizRequiredKey from './model/QuizRequiredKey';
import QuizDetailView from '../QuizDetail/QuizDetailView';


interface Props {
  groupId: string;
  quizSheetId: string;
  writer: IdName;
  limit?: number;
  onSuccess?: () => {};
  onFail?: () => {};
}

interface State {
}

interface InjectedProps {
  //
  quizzesStateKeeper: QuizzesStateKeeper;
  quizStateKeeper: QuizStateKeeper;
}

@inject(QuizzesStateKeeper.instanceName, QuizStateKeeper.instanceName)
@autobind
@observer
class QuizzesDetailContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    limit: 20,
    onSuccess: () => {},
    onFail: () => {},
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { quizSheetId: prevQuizSheetId } = prevProps;
    const { quizSheetId } = this.props;

    if (prevQuizSheetId !== quizSheetId) {
      this.init();
    }
  }

  async init() {
    //
    const { quizSheetId } = this.props;
    const { limit } = this.propsWithDefault;
    const { quizzesStateKeeper } = this.injected;

    const targetOffset = Offset.newAscending(0, limit, 'no');

    if (!quizSheetId) {
      return;
    }

    const quizzes = await quizzesStateKeeper.findQuizzesByQuizSheetId(quizSheetId, targetOffset);

    if (quizzes.length === 0) {
      this.addQuiz();
    }
  }

  onClickModify(event: React.MouseEvent, index: number) {
    //
    const { quizzesStateKeeper } = this.injected;

    quizzesStateKeeper.setQuizProp(index, 'editing', true);
  }

  onChange(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number) {
    //
    const { quizzesStateKeeper } = this.injected;

    const name = event.target.name as keyof Quiz;
    const value = event.target.value;

    quizzesStateKeeper.setQuizProp(index, name, value);
  }

  onChangeCategory(event: React.ChangeEvent<{ name?: string; value: unknown }>, index: number) {
    //
    const { quizzesStateKeeper } = this.injected;

    const name = event.target.name as keyof Quiz;
    const value = event.target.value;

    quizzesStateKeeper.setQuizProp(index, name, value);
  }

  onChangeQuizItem(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number, quizItemIndex: number) {
    //
    const { quizzesStateKeeper } = this.injected;

    const name = event.target.name as keyof QuizItem;
    const value = event.target.value;

    quizzesStateKeeper.setQuizItemProp(index, quizItemIndex, name, value);
  }

  addQuizItem(index: number) {
    //
    const { quizzesStateKeeper } = this.injected;

    const quizzes = quizzesStateKeeper.quizzes;
    const newQuizItem = QuizItem.new();

    newQuizItem.sequence = quizzes[index].quizItems.length + 1;

    quizzesStateKeeper.addQuizItem(newQuizItem, index);
  }

  addQuiz() {
    //
    const { groupId, quizSheetId, writer } = this.props;
    const { quizzesStateKeeper } = this.injected;

    const newQuiz = Quiz.newDefault(groupId, quizSheetId, writer);

    newQuiz.writer = writer;

    quizzesStateKeeper.addQuiz(newQuiz);
  }

  async onRemoveQuiz(event: React.MouseEvent, removedIndex: number) {
    //
    const { quizStateKeeper, quizzesStateKeeper } = this.injected;
    const targetQuiz = quizzesStateKeeper.quizzes[removedIndex];
    const { quizzes } = quizzesStateKeeper;

    const filteredQuizzes = quizzes.filter((quiz, index) => index !== removedIndex);

    if (targetQuiz.id) {
      const response = await quizStateKeeper.remove(targetQuiz.id);

      if (response.entityIds.length > 0) {
        quizzesStateKeeper.setQuizzes(filteredQuizzes);
      }
    }

    quizzesStateKeeper.setQuizzes(filteredQuizzes);
  }

  async onSubmit(event: React.MouseEvent, index: number) {
    //
    const { onSuccess, onFail } = this.propsWithDefault;
    const { quizzesStateKeeper, quizStateKeeper } = this.injected;
    const { quizzes } = quizzesStateKeeper;

    let commandResponse;
    const targetQuiz = quizzes[index];

    if (!targetQuiz) {
      throw new Error('Quiz should not be null');
    }

    const isValid = this.validationCheck(targetQuiz);

    if (!isValid) {
      return;
    }

    if (targetQuiz.id.length === 0) {
      commandResponse = await quizStateKeeper.save(targetQuiz);
    }
    else {
      commandResponse = await quizStateKeeper.modify(targetQuiz.id, Quiz.asNameValues(targetQuiz));
    }

    if (commandResponse.entityIds.length) {
      onSuccess();

    }
    else {
      onFail();
    }

    quizzesStateKeeper.setQuizProp(index, 'editing', false);
  }

  validationCheck(quiz: Quiz) {
    //
    const requiredKeys = Object.values(QuizRequiredKey).map(key => key) as (keyof Quiz)[];
    const quizValidation = validationUtils.checkEmpty<Quiz, keyof Quiz>(
      quiz,
      requiredKeys
    );

    if (!quizValidation.valid) {
      const requiredKeys = quizValidation.invalidKeys as QuizRequiredKey[];

      alert(`${QuizKeyName[requiredKeys[0]]}을 입력해주세요.`);
      return false;
    }
    const quizItemsValidations = quiz.quizItems.map(quizItem => validationUtils.checkEmpty<QuizItem, keyof QuizItem>(quizItem, ['itemText']));

    const filteredValidation = quizItemsValidations.find(quizItemsValidation => !quizItemsValidation.valid);

    if (filteredValidation) {
      alert(`${QuizKeyName[QuizRequiredKey.quizItems]}을 입력해주세요.`);
      return false;
    }

    return true;
  }

  onClickRemoveQuizItem(quizIndex: number, quizItemIndex: number) {
    const { quizzesStateKeeper } = this.injected;

    quizzesStateKeeper.substractQuizItem(quizIndex, quizItemIndex);
  }

  render() {
    //
    const { quizzesStateKeeper } = this.injected;
    const { quizzes } = quizzesStateKeeper;

    if (!quizzes) {
      return null;
    }

    return (
      <>
        <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
          <Box mr={1}>
            <Typography color="textSecondary" gutterBottom>총 {quizzes.length}개</Typography>
          </Box>
        </Box>
        <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
          <Box mr={1}>
            <Button variant="contained" onClick={() => this.addQuiz()}>
              문제 추가<Add fontSize="small" />
            </Button>
          </Box>
        </Box>
        {quizzes.length > 0 ?
          quizzes.map((quiz, index) => (
            <QuizDetailView
              key={'quiz-' + index}
              quiz={quiz}
              index={index}
              onChange={this.onChange}
              onChangeCategory={this.onChangeCategory}
              onChangeQuizItem={this.onChangeQuizItem}
              onSubmit={this.onSubmit}
              onClickModify={this.onClickModify}
              addQuizItem={this.addQuizItem}
              onRemoveQuiz={this.onRemoveQuiz}
              onClickRemoveQuizItem={this.onClickRemoveQuizItem}
            />
          )) : null
        }
      </>
    );
  }
}

export default QuizzesDetailContainer;

