import React from 'react';
import { observer } from 'mobx-react';
import { FormControl, Button, InputLabel, Select, MenuItem, TextField, Box } from '@material-ui/core';
import { QuizSheet, QuizSheetCategory } from '~/comp/api/quiz';
import { ReactComponent } from '~/comp/shared';


interface Props {
  quizSheet: QuizSheet;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onSubmit: (event: React.MouseEvent) => void;
  onChangeCategory: (event: React.ChangeEvent<{ name?: string; value: unknown }>) => void;
}

@observer
class QuizSheetFormView extends ReactComponent<Props> {
  //
  render() {
    const { quizSheet, onChange, onSubmit, onChangeCategory } = this.props;

    return (
      <>
        <TextField
          required
          fullWidth
          label="title"
          name="title"
          value={quizSheet.title}
          onChange={onChange}
        />
        <TextField
          required
          fullWidth
          label="subject"
          name="subject"
          value={quizSheet.subject}
          onChange={onChange}
        />
        <FormControl fullWidth>
          <InputLabel id="quizSheetCategory">QuizSheetCategory</InputLabel>
          <Select
            id="quizSheetCategory"
            label="quizSheetCategory"
            name="quizSheetCategory"
            value={quizSheet.quizSheetCategory || ''}
            onChange={(event) => onChangeCategory(event)}
          >
            {
              Object.values(QuizSheetCategory).map(category => (
                <MenuItem value={category}>{category}</MenuItem>
              ))
            }
          </Select>
        </FormControl>
        <Box display="flex" justifyContent="center" mt={2} mb={1}>
          <Box mr={1}>
            <Button
              color="primary"
              variant="outlined"
              onClick={onSubmit}
            >
              완료
            </Button>
          </Box>
        </Box>
      </>
    );
  }
}

export default QuizSheetFormView;
