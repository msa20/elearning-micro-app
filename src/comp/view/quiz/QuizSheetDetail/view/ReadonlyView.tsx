import React from 'react';
import { observer } from 'mobx-react';
import {
  Box, Button,
  Grid,
  Typography,
} from '@material-ui/core';
import { QuizSheet, QuizSheetStateName } from '~/comp/api';
import { ReactComponent } from '~/comp';


interface Props {
  quizSheet: QuizSheet;
  onClickEdit: (event: React.MouseEvent) => void;
  onClickFinish: (event: React.MouseEvent) => void;
}

@observer
class ReadonlyView extends ReactComponent<Props> {
  //
  render() {
    const { quizSheet, onClickEdit, onClickFinish } = this.props;

    return (
      <>
        <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
          <Box mr={1}>
            <Button
              color="primary"
              variant="outlined"
              onClick={onClickEdit}
            >
              수정
            </Button>
          </Box>
          <Box>
            <Button
              color="primary"
              variant="outlined"
              onClick={onClickFinish}
            >
              출제
            </Button>
          </Box>
        </Box>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <Typography align="center">출제자 : {quizSheet.writerName}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography align="center">시험지 상태 : {QuizSheetStateName[quizSheet.quizSheetState]}</Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography align="center">시험 제목 : {quizSheet.title}</Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography align="center">시험 주제 : {quizSheet.subject}</Typography>
          </Grid>
          <Grid item xs={4}>
            <Typography align="center">시험 카테고리 : {quizSheet.quizSheetCategory}</Typography>
          </Grid>
        </Grid>
      </>
    );
  }
}


export default ReadonlyView;
