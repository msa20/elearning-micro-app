import React from 'react';
import { observer } from 'mobx-react';
import {
  Box,
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import { QuizSheet, QuizSheetCategory, QuizSheetStateName } from '~/comp/api';
import { ReactComponent } from '~/comp';


interface Props {
  quizSheet: QuizSheet;
  onChangeQuizSheet: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onChangeCategory: (event: React.ChangeEvent<{ name?: string; value: unknown }>) => void;
  onClickModify: (event: React.MouseEvent) => void;
}

@observer
class EditableView extends ReactComponent<Props> {
  //
  render() {
    const { quizSheet, onChangeQuizSheet, onChangeCategory, onClickModify } = this.props;

    return (
      <>
        <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
          <Box mr={1}>
            <Button
              color="primary"
              variant="outlined"
              onClick={onClickModify}
            >
              시험지 저장
            </Button>
          </Box>
        </Box>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <Typography align="center">출제자 : {quizSheet.writerName}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography align="center">시험지 상태 : {QuizSheetStateName[quizSheet.quizSheetState]}</Typography>
          </Grid>
          <Grid item xs={4}>
            <TextField
              required
              fullWidth
              label="title"
              name="title"
              value={quizSheet.title}
              placeholder={quizSheet.title}
              onChange={onChangeQuizSheet}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              required
              fullWidth
              label="subject"
              name="subject"
              value={quizSheet.subject}
              placeholder={quizSheet.subject}
              onChange={onChangeQuizSheet}
            />
          </Grid>
          <Grid item xs={4}>
            <FormControl fullWidth>
              <InputLabel id="quizSheetCategory">QuizSheetCategory</InputLabel>
              <Select
                id="quizSheetCategory"
                label="quizSheetCategory"
                name="quizSheetCategory"
                placeholder={quizSheet.quizSheetCategory || ''}
                value={quizSheet.quizSheetCategory || ''}
                onChange={(event) => onChangeCategory(event)}
              >
                {
                  Object.values(QuizSheetCategory).map((category, index) => (
                    <MenuItem key={'category-' + index} value={category}>{category}</MenuItem>
                  ))
                }
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </>
    );
  }
}


export default EditableView;
