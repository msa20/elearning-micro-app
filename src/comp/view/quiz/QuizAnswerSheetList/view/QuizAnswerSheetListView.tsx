import React from 'react';
import { observer } from 'mobx-react';
import { Box, Table, Typography, TableRow, TableCell, TableBody, TableHead, Grid } from '@material-ui/core';
import { QuizAnswerSheet } from '~/comp/api';
import { autobind, IdName, ReactComponent } from '~/comp/shared';


interface Props {
  writer: IdName;
  quizAnswerSheets: QuizAnswerSheet[];
  onClick: (event: React.MouseEvent, answer: QuizAnswerSheet) => void;
}

@autobind
@observer
class QuizAnswerSheetListView extends ReactComponent<Props> {
  //
  render() {
    const { writer, quizAnswerSheets, onClick } = this.props;

    return (
      <>
        <Grid container>
          <Grid item xs={6}>
            <Typography color="textSecondary" gutterBottom>총 {quizAnswerSheets.length}개</Typography>
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Box pb={1} textAlign="right" />
        </Grid>
        <Box mb={2}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell variant="head" align="center">
                  <Typography>주제</Typography>
                </TableCell>
                <TableCell variant="head" align="center">
                  <Typography>응시자</Typography>
                </TableCell>
                <TableCell variant="head" align="center">
                  <Typography>결과</Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {quizAnswerSheets.length ? quizAnswerSheets.map((quizAnswerSheet) => (
                <TableRow key={quizAnswerSheet.id} onClick={(event: React.MouseEvent) => onClick(event, quizAnswerSheet)}>
                  <TableCell align="center">
                    <Typography>{quizAnswerSheet.subject}</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography>{writer.name}</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography>{quizAnswerSheet.passed ? 'Pass' : 'Fail'}</Typography>
                  </TableCell>
                </TableRow>
              )) : (
                <TableRow>
                  <TableCell colSpan={4} align="center">등록된 답안지가 없습니다.</TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Box>
      </>
    );
  }
}

export default QuizAnswerSheetListView;
