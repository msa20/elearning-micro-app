import React from 'react';
import { observer } from 'mobx-react';
import { Button, FormControl, Grid, InputLabel, MenuItem, Select } from '@material-ui/core';
import { QuizSheet } from '~/comp/api';
import { ReactComponent } from '~/comp/shared';


interface Props {
  quizSheets: QuizSheet[];
  onClickQuizSheet: (event: React.ChangeEvent<{ name?: string; value: unknown }>) => void;
  onClick: (event: React.MouseEvent) => void;
}

@observer
class QuizSheetPublishedListView extends ReactComponent<Props> {
  //
  render() {
    const { quizSheets, onClick, onClickQuizSheet } = this.props;

    return (
      <Grid container>
        <Grid item xs={10}>
          <FormControl fullWidth>
            <InputLabel>시험지를 선택하세요.</InputLabel>
            <Select
              id="quizSheetId"
              labelId="quizSheetId"
              onChange={((event) => onClickQuizSheet(event))}
            >
              {
                quizSheets.map(quizSheet => (
                  <MenuItem value={quizSheet.id}>{quizSheet.title}</MenuItem>
                ))
              }
            </Select>

          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <Button size="medium" onClick={onClick}>응시하기</Button>
        </Grid>
      </Grid>

    );
  }
}

export default QuizSheetPublishedListView;
