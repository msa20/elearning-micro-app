import QuizFormBaseView from './QuizFormBaseView';
import Content from './sub-comp/Content';
import Actions from './sub-comp/Actions';


type QuizFormBaseComponent = typeof QuizFormBaseView & {
  Content: typeof Content;
  Actions: typeof Actions;
};

const QuizFormBase = QuizFormBaseView as QuizFormBaseComponent;

QuizFormBase.Content = Content;
QuizFormBase.Actions = Actions;

export default QuizFormBase;
