import React from 'react';
import { Helmet } from 'react-helmet';
import { Box, Container, Grid, Typography } from '@material-ui/core';
import HeaderImage from '../../../HeaderImage';
import { nWithStyles, NWithStyles } from '../../../../nStyle';
import { ReactComponent } from '../../../../module';


interface Props extends NWithStyles {
  title: React.ReactNode;
  description: React.ReactNode;
  img?: string;
  action?: React.ReactNode;
  hidden?: boolean;
  className?: string;
}


class PageLayoutHeaderView extends ReactComponent<Props> {
  //
  static defaultProps = {
    title: null,
    description: null,
    img: '',
    action: null,
    hidden: false,
    className: '',
  };


  render() {
    //
    const { classes, className, hidden, title, description, img, action } = this.props;

    return (
      <>
        <Helmet>
          <title>{title}</title>
        </Helmet>

        {!hidden && (
          <Box className={classes.ntr_sline}>
            <Container className={classes.nmax_wrap}>
              <Box pb={4}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Box display="flex" mt={6} alignContent="center" className={classes.nsum_wrap}>
                      <Box mr={5}>
                        <HeaderImage img={img} className={className} />
                      </Box>
                      <Box mt={3}>
                        <Typography paragraph variant="h2">{title}</Typography>
                        <Typography style={{ whiteSpace: 'pre-line' }}>
                          {description}
                        </Typography>
                      </Box>
                      <Box className={classes.nsum_btn}>
                        {action}
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Container>
          </Box>
        )}
      </>
    );
  }
}

export default nWithStyles(PageLayoutHeaderView);
