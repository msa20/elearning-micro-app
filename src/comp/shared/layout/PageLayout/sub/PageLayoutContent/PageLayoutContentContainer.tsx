import React from 'react';
import { Box } from '@material-ui/core';
import { ReactComponent } from '../../../../module';


interface Props {
  children: React.ReactNode;
}

class PageLayoutContentContainer extends ReactComponent<Props> {
  //
  render() {
    //
    const { children } = this.props;

    return (
      <Box>
        {children}
      </Box>
    );
  }
}

export default PageLayoutContentContainer;
