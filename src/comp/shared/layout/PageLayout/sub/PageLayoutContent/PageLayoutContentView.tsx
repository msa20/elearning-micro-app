import React from 'react';
import { Box } from '@material-ui/core';
import { ReactComponent } from '../../../../module';


interface Props {
  children: React.ReactNode;
}

class PageLayoutContentView extends ReactComponent<Props> {
  //
  render() {
    //
    const { children } = this.props;

    return (
      <Box mt={5}>
        {children}
      </Box>
    );
  }
}

export default PageLayoutContentView;
