import React from 'react';
import { Box } from '@material-ui/core';
import { ReactComponent } from '../../../../module';


interface Props {
  children: React.ReactNode;
}

class PageLayoutContentHeaderView extends ReactComponent<Props> {
  //
  render() {
    //
    const { children } = this.props;

    return (
      <Box mb={5}>
        {children}
      </Box>
    );
  }
}

export default PageLayoutContentHeaderView;
