import PageLayoutContainer from './PageLayoutContainer';
import PageLayoutHeader from './sub/PageLayoutHeader';
import PageLayoutBody from './sub/PageLayoutBody';
import PageLayoutContent from './sub/PageLayoutContent';


type PageLayoutType = typeof PageLayoutContainer & {
  Header: typeof PageLayoutHeader;
  Body: typeof PageLayoutBody;
  Content: typeof PageLayoutContent;
};

const PageLayout = PageLayoutContainer as PageLayoutType;

PageLayout.Header = PageLayoutHeader;
PageLayout.Body = PageLayoutBody;
PageLayout.Content = PageLayoutContent;

export default PageLayout;
