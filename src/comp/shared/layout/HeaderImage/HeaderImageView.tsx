import React from 'react';
import clsx from 'clsx';
import { WithStyles, withStyles } from './style';
import { ReactComponent } from '~/comp/shared/module';


interface Props extends WithStyles {
  img?: string;
  className?: string;
}

class HeaderImageView extends ReactComponent<Props> {
  //
  static defaultProps = {
    img: '',
    className: '',
  };


  render() {
    //
    const { classes, className } = this.props;

    return (
      <div className={clsx(classes.ntr_simg, className)} />
    );
  }
}

export default withStyles(HeaderImageView);
