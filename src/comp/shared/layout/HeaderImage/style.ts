import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


type NtrSimgType = { img?: string };

const style = (theme: Theme) => createStyles({
  ntr_simg: {
    width: '211px',
    height: '147px',
    background: (props: NtrSimgType) => (props.img ? `url("${props.img}")no-repeat center top` : ''),
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
