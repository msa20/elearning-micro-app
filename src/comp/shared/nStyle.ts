import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const basePath = process.env.BASE_PATH;

const style = (theme: Theme) => createStyles({
  /* 서브 상단 이미지 S */
  nsum_btn: {
    position: 'absolute',
    right: '0px',
    top: '50px',
  },
  ntr_sline: {
    borderBottom: '1px solid #dcdcdc',
  },
  nmax_wrap: {
    maxWidth: '1568px',
  },
  nsum_wrap: {
    position: 'relative',
  },
  ntr_simg07: {
    width: '211px',
    height: '147px',
    background: `url("${basePath}/images/icon/ntr_simg07.png")no-repeat center top`,
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
});

export default style;
export type NWithStyles = MuiWithStyles<typeof style>;
export const nWithStyles = muiWithStyles(style);
