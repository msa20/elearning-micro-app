
class OffsetElementList<T> {
  //
  empty: boolean;
  totalCount: number;
  results: T[] = [];


  constructor(results: T[], totalCount: number) {
    //
    this.results = results;
    this.totalCount = totalCount;
    this.empty = false;
  }
}

export default OffsetElementList;
