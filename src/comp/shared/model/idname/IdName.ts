
import { makeAutoObservable } from 'mobx';


class IdName {
  //
  id: string;
  name: string;
  usid: string;


  constructor(id: string = '', name: string = '', usid: string = '') {
    //
    makeAutoObservable(this);

    this.id = id;
    this.name = name;
    this.usid = usid;
  }

  static fromDomain(domain: IdName): IdName {
    //
    return new IdName(domain.id, domain.name);
  }

  static fromModel(model: any): IdName {
    //
    return new IdName(model.id, model.name, model.usid);
  }

  [key: string]: any;
}

export default IdName;
